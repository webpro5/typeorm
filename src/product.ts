import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productsRopository = AppDataSource.getRepository(Product);
   

    console.log("Loading products from the database...")
    const products = await productsRopository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productsRopository.findOneBy({ id:1})
    console.log(updateProduct)
    updateProduct.price =80 ;
    await productsRopository.save(updateProduct)

    

}).catch(error => console.log(error))
